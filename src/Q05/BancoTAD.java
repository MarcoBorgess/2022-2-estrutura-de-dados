package Q05;

public class BancoTAD implements Banco{

    private double saldo;

    public BancoTAD(double saldo){
        this.saldo = saldo;
    }

    @Override
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public double getSaldo() {
        return saldo;
    }

    @Override
    public String depositar(double valor) {
        saldo += valor;
        return "Novo saldo: R$" + saldo;
    }

    @Override
    public String sacar(double valor) {
        if (saldo >= valor){
            saldo -= valor;
            return "Novo saldo: R$" + saldo;
        }else return "Saldo insuficiente";
    }

    @Override
    public String atualizar() {
        return "Saldo: R$" + getSaldo();
    }
}
