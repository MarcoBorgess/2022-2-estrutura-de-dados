package Q05;

public interface Banco {
    void setSaldo(double saldo);
    double getSaldo();
    String depositar(double valor);
    String sacar(double valor);
    String atualizar();
}
