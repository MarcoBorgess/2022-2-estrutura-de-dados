package Q03;

public class ProfessorTAD implements Professor {
    private String nome;
    private int codigoDisciplina;

    public ProfessorTAD(String nome, int codigoDisciplina) {
        this.nome = nome;
        this.codigoDisciplina = codigoDisciplina;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public void setCodigoDisciplina(int codigoDisciplina) {
        this.codigoDisciplina = codigoDisciplina;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public int getCodigoDisciplina() {
        return codigoDisciplina;
    }
}
