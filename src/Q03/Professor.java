package Q03;

public interface Professor {
    void setNome(String nome);
    void setCodigoDisciplina(int codigoDisciplina);
    String getNome();
    int getCodigoDisciplina();
}
