package Q06;

public interface Agenda {
    void setTelefones(String[] telefones);
    String[] getTelefones();
    String addTelefone(String telefone);
    String excluirTelefone(String telefone);
    String atualizarLista();
}
