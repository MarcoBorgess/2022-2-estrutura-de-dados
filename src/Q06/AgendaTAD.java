package Q06;

public class AgendaTAD implements Agenda{

    private String[] telefones;

    public AgendaTAD(String[] telefones){
        telefones = telefones;
    }

    @Override
    public void setTelefones(String[] telefones) {
        this.telefones = telefones;
    }

    @Override
    public String[] getTelefones() {
        return telefones;
    }

    @Override
    public String addTelefone(String telefone) {
        for (String tel : telefones){
            if(telefone.equals(tel)){
                return "Telefone já se encontra na agenda";
            }
        }
        for (String tel : telefones){
            if (tel.isEmpty()) {
                tel = telefone;
                return "Adicionado o telefone " + telefone;
            }
        }
        return "Não foi possivel adicionar o telefone";
    }

    @Override
    public String excluirTelefone(String telefone) {
        for (String tel : telefones){
            if(telefone.equals(tel)){
                tel = null;
                return "Telefone foi excluido com sucesso";
            }
        }
        return "Não foi possivél excluir o telefone informado";
    }

    @Override
    public String atualizarLista() {
        String txt = "Telefones: ";
        for (String telefone : telefones){
            txt += ("\n" + telefone);
        }
        return txt;
    }

}
