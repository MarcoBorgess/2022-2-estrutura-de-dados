package Q02;

public class AlunoTAD implements Aluno {
    private String nome;
    private int semestre;

    public AlunoTAD(String nome, int semestre) {
        this.nome = nome;
        this.semestre = semestre;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public int getSemestre() {
        return semestre;
    }
}
