package Q02;

public interface Aluno {
    void setNome(String nome);
    void setSemestre(int semestre);
    String getNome();
    int getSemestre();
}
