package Q01;

public interface Livro {
    void setNome(String nome);
    void setEditora(String editora);
    void setAno(int ano);
    String getNome();
    String getEditora();
    int getAno();
}
