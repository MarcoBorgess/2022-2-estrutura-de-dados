package Q01;

public class LivroTAD implements Livro {

    private String nome;
    private String editora;
    private int ano;

    public LivroTAD(String nome, String editora, int ano){
        this.nome = nome;
        this.editora = editora;
        this.ano = ano;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public void setEditora(String editora) {
        this.editora = editora;
    }

    @Override
    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public String getEditora() {
        return editora;
    }

    @Override
    public int getAno() {
        return ano;
    }
}
