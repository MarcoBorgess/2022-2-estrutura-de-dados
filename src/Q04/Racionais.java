package Q04;

public interface Racionais {
    void setRacional(double racional);
    void setRacionalDois(double racionalDois);
    double getRacionalDois();
    double getRacional();
    double somaRacional(double racional, double racionalDois);
    double multiplicaRacional(double racional, double racionalDois);
    boolean testarIgualdade(double racional, double racionalDois);
}
