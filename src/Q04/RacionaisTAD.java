package Q04;

public class RacionaisTAD implements Racionais {

    private double racional;
    private double racionalDois;

    public RacionaisTAD(double racional, double racionalDois){
        this.racional = racional;
        this.racionalDois = racionalDois;
    }

    @Override
    public void setRacional(double racional) {
        this.racional = racional;
    }

    @Override
    public void setRacionalDois(double racionalDois) {
        this.racionalDois = racionalDois;
    }

    @Override
    public double getRacional() {
        return racional;
    }

    @Override
    public double getRacionalDois() {
        return racionalDois;
    }

    @Override
    public double somaRacional(double racional, double racionalDois) {
        return racional+racionalDois;
    }

    @Override
    public double multiplicaRacional(double racional, double racionalDois) {
        return racional*racionalDois;
    }

    @Override
    public boolean testarIgualdade(double racional, double racionalDois) {
        if(racional == racionalDois){
            return true;
        }else return false;
    }
}
